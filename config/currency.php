<?php

return [
    'apiUrl' => env('CURRENCY_API_ENDPOINT'),
    'apiKey' => env('CURRENCY_API_KEY')
];
