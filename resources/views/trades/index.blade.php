@extends('layouts.app')

@include('partials.content-index-header')

@section('content')
    <div class=" container-fluid browse">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                 @include('offers.table')
            </div>
        </div>
    </div>
@endsection

