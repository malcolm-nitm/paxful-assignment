@extends('layouts.app')

@include('partials.content-edit-add-header')

@section('content')
    <div class="page-content container-fluid browse">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($model, ['route' => ['offers.update', $model->id], 'method' => 'patch']) !!}

                        @include('offers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
    </div>
@endsection
