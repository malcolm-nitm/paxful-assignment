<table class="table table-responsive" id="offers-table">
    <thead>
        <th>By</th>
        <th>Payment Method</th>
        <th>Min</th>
        <th>Max</th>
        <th>BTC Price</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($models as $model)
        <tr>
            <td>{!! $model->user->name !!}</td>
            <td>{!! $model->paymentMethod->name !!}</td>
            <td>{!! $model->min !!}</td>
            <td>{!! $model->max !!}</td>
            <td>{!! $model->final_price !!}</td>
            <td>
                {!! Form::open(['route' => ['offers.destroy', $model->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('offers.show', [$model->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('offers.edit', [$model->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
