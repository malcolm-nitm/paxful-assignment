<ul class="list-group">
    <li class="list-group-item">
        ID
        <span class="badge badge-primary badge-pill">{{ $model->id }}</span>
    </li>
    <li class="list-group-item">
        Final Price
        <span class="badge badge-primary badge-pill">{{ $model->finalPrice }}</span>
    </li>
    <li class="list-group-item">
        Markup on Original
        <span class="badge badge-primary badge-pill">{{ $model->markup }}</span>
    </li>
    <li class="list-group-item">
        Currency
        <span class="badge badge-primary badge-pill">{{ $model->currency->code }}</span>
    </li>
    <li class="list-group-item">
        Payment Method
        <span class="badge badge-primary badge-pill">{{ $model->paymentMethod->name }}</span>
    </li>
    <li class="list-group-item">
        Min Acceptable Offer
        <span class="badge badge-primary badge-pill">{{ $model->amount_min }}</span>
    </li>
    <li class="list-group-item">
        Max Acceptable OFfer
        <span class="badge badge-primary badge-pill">{{ $model->amount_max }}</span>
    </li>
</ul>
