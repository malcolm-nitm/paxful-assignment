<!-- Currency Field -->
<div class="form-group col-sm-12">
    {!! Form::label('currency_id', 'Currency:') !!}
    {!! Form::select('currency_id', \App\Entities\Currency::limit(10)->pluck('currency', 'id'), null, [
        'class' => 'form-control select2',
        'onchange' => 'helpers.calculatePrice()'
    ]) !!}
</div>

<!-- Payment Method Field -->
<div class="form-group col-sm-12">
    {!! Form::label('payment_method_id', 'Payment Method:') !!}
    {!! Form::select('payment_method_id', \App\Entities\PaymentMethod::limit(10)->pluck('name', 'id'), null, [
        'class' => 'form-control select2'
    ]) !!}
</div>

<!-- Minimum Amount Field -->
<div class="form-group col-sm-12">
    {!! Form::label('amount_min', 'Min accepable amount:') !!}
    {!! Form::number('amount_min', null, [
        'class' => 'form-control',
        'onchange' => 'helpers.calculatePrice',
        'step' => '0.00000001',
        'placeholder' => "Minimum amount you'll accept"
    ]) !!}
</div>

<!-- Minimum Amount Field -->
<div class="form-group col-sm-12">
    {!! Form::label('amount_max', 'Max acceptable amount:') !!}
    {!! Form::number('amount_max', null, [
        'class' => 'form-control',
        'onchange' => 'helpers.calculatePrice()',
        'step' => '0.00000001',
        'placeholder' => "Maximum amount you'll accept"
    ]) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('markup', 'Markup:') !!}
    <div class="input-group">
        <span class="input-group-text">percentage</span>
            {!! Form::number('markup', null, [
                'class' => 'form-control',
                'onchange' => 'helpers.calculatePrice()',
                'min' => 0,
                'placeholder' => 'Markup percentage: 0+'
            ]) !!}
    </div>
</div>

<div class="form-group col-sm-12">
    {!! Form::label('final_price', 'Final Price:') !!}
    <div class="input-group">
        <span id="final_price_currency" class="input-group-text"></span>
        {!! Form::text('final_price', null, ['class' => 'form-control', 'readonly' => true]) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('home') !!}" class="btn btn-default">Cancel</a>
</div>
