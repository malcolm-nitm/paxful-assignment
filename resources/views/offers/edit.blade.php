@extends('layouts.app')

@include('partials.content-edit-add-header')

@section('content')
<div class="container-fluid">
    <div class="card card-primary">
        <div class="card-body">
            {!! Form::model($model, ['route' => ['offers.update', $model->id], 'method' => 'patch']) !!}

                @include('offers.fields')

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
