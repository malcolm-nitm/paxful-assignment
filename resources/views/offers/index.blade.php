@extends('layouts.app')

@include('partials.content-index-header')

@section('content')
<div class="card card-primary">
    <div class="card-body">
        @include('offers.table')
    </div>
</div>
@endsection

