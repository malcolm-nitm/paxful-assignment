@extends('layouts.app')

@include('partials.content-edit-add-header')

@section('content')
    <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-body">
                {!! Form::open(['route' => 'offers.store', 'class' => 'form']) !!}

                    @include('offers.fields')

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
