@extends('layouts.app')

@include('partials.content-index-header')

@section('content')
    <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-body">
                @include('offers.show_fields')
            </div>
        </div>
    </div>
@endsection
