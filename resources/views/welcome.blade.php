@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h1>Current Offers</h1>
        @include('partials.offers')
    </div>
</div>
@endsection

