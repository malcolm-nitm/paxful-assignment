@extends('layouts.app')

@section('content')
<div class="card mb-3">
    <div class="card-header display-4">Dashboard</div>

    <div class="card-body">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        <div class="jumbotron">
            <h1 class="display-5">Hello {{auth()->user()->name}}!</h1>
            <span class="display-3">Bitcoins: </span><span class="display-4">{{auth()->user()->balance->balance}}</span>
        </div>
    </div>
</div>
<div class="card mb-3">
    <div class="card-header row align-items-center justify-content-between">
        <span class="display-4">Offers</span>
        <a href="/offers/create" class="display-5">Create an offer</a>
    </div>

    <div class="card-body">
        @include('partials.offers')
    </div>
</div>
<div class="card">
    <div class="card-header row align-items-center justify-content-between">
        <span class="display-4">Trades</span>
        <a href="/" class="display-5">Start a trade</a>
    </div>

    <div class="card-body">
        @include('partials.trades')
    </div>
</div>
@endsection
