<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Owner</th>
            <th>Payment Method</th>
            <th>Min/Max</th>
            <th>BTC Price</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @forelse ($offers as $offer)
        <tr>
            <td>{{$offer->user->name}}</td>
            <td>{{$offer->paymentMethod->name}}</td>
            <td>{{$offer->amount_min}} / {{$offer->amount_max}}</td>
            <td>{{$offer->fomattedFinalPrice}}</td>
            <td class="col-sm-4">
                @if(auth()->check() && !auth()->user()->ownsOffer($offer))
                {!! Form::open(['route' => 'trades.store', 'class' => 'form']) !!}
                    <div class="form-group mb-0">
                        {!! Form::hidden('offer_id', $offer->id) !!}
                        {!! Form::number('fiat_amount', $offer->amount_min, [
                            'class' => 'form-control',
                            'step' => '0.00000001',
                            'placeholder' => "Your Offer",
                            'min' => $offer->amount_min,
                            'max' => $offer->amount_max,
                            'value' => $offer->amount_min
                        ]) !!}
                    </div>
                    <div class="form-group row mx-0">
                        <div class="input-group col-sm-12 mx-0 px-0">
                            <div class="input-group-prepend col-sm-6 px-0">
                                <span class="btn btn-info btn-block rounded-bottom">{{$offer->currency->code}}</span>
                            </div>
                            <div class="input-group-append col-sm-6 px-0">
                            <button id="buy{{$offer->id}}" type="submit" class="btn btn-block rounded-bottom" >Buy</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                @elseif(auth()->check() && auth()->user()->ownsOffer($offer))
                <div class="btn-group">
                    <a class="btn btn-info" href="{{route('offers.edit', ['id' => $offer->id])}}">Edit</a>
                    <form method="POST" onsubmit="return confirm('Are you sure?')" action="{{route('offers.destroy', ['id' => $offer->id])}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                    <a class="btn {{$offer->is_active ? 'btn-disabled' : 'btn-info'}}" href="{{route('offers.disable', ['id' => $offer->id])}}">{{$offer->is_active ? 'Disable' : 'Enable'}}</a>
                </div>
                @endif
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="5">
                No offers!
                @if(!auth()->guest())
                    <a href="/offers/create" class="display-5">Create an offer</a>
                @endif
            </td>
        </tr>
        @endforelse
    </tbody>
</table>
