@section('header')
    <div class="row align-items-center justify-content-start">
        <div class="col-sm-2">
            <a href="{{ url()->previous() }}" class="btn">Back</a>
        </div>
        <div class="col-sm-10">
            <h1 class="justify-self-center text-center">
                {{title_case(str_replace('.', ' ', request()->route()->getName()))}}
            </h1>
        </div>
    </div>
@endsection;
