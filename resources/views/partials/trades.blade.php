<table class="table">
    <thead>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @forelse ($trades as $trade)
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        @empty
        <tr>
            <td colspan="5">
                No trades!
                @if(!auth()->guest())
                    <a href="/" class="display-5">Accept an available offer</a>
                @endif
            </td>
        </tr>
        @endforelse
    </tbody>
</table>
