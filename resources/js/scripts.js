// No interest in building vue app due to time constraints
const calculatePrice = function () {
    const currency = document.querySelector('#currency_id').value;
    const method = document.querySelector('#payment_method_id').value;
    const min = document.querySelector('#amount_min').value;
    const max = document.querySelector('#amount_max').value;
    const markup = document.querySelector('#markup').value;
    const data = {
        currency,
        method,
        min,
        max,
        markup
    };
    fetch('/api/offers/calculate-price', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            // "Content-Type": "application/x-www-form-urlencoded",
        },
        body: JSON.stringify(data)
    }).then(response => {
        response.json().then(result => {
            const finalPrice = document.querySelector('#final_price');
            const currency = document.querySelector('#final_price_currency');
            finalPrice.value = result.value;
            currency.innerHTML = result.currency;
        });
    })
}

const checkOfferValue = function (input, offerId, buttonId) {
    const data = {
        offer_id: offerId,
        fiat_amount: input.value,
    };
    fetch('/api/trades/check-offer', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            // "Content-Type": "application/x-www-form-urlencoded",
        },
        body: JSON.stringify(data)
    }).then(response => {
        response.json().then(result => {
            document.querySelector(buttonId).classList.remove('disabled').add('btn-success');
        });
    }).catch(error => {
        alert(JSON.stringify(error));
    })
}

function deleteItem(url, body) {
    if (confirm('Are you sure?')) {
        return fetch(url, {
            method: 'DELETE',
            body: body || {}
        })
    }
    return false;
}

window.helpers = {
    calculatePrice,
    deleteItem,
    checkOfferValue
};
