<?php

use App\Entities\Currency;
use App\Entities\PaymentMethod;
use App\Entities\Offer;
use Faker\Generator as Faker;

$factory->define(Offer::class, function (Faker $faker) {
    $min = $faker->randomFloat(8);
    // Let's not make the max amount too high
    $max = $faker->randomFloat(8, $min, $min * 1.5);
    $currency = Currency::inRandomOrder()->first();
    $offer = new Offer;
    $markup = $faker->numberBetween(0, 100);
    $finalPrice = $offer->calculate($currency->id, $markup);
    return [
        'currency_id' => $currency->id,
        'payment_method_id' => PaymentMethod::inRandomOrder()->first()->id,
        'amount_min' => $min,
        'amount_max' => $max,
        'markup' => $markup,
        'final_price' => $finalPrice['value'],
        'is_active' => $faker->boolean()
    ];
});
