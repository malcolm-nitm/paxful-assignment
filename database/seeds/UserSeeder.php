<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 5)->create()->each(function ($user) {
            factory(App\Entities\Offer::class, 5)->make()->each(function ($offer) use ($user) {
                $user->offer_id = $user->id;
                $user->offers()->save($offer);
            });
        });
    }
}
