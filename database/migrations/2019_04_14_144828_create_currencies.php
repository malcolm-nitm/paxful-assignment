<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Facades\App\Helpers\Currency;

class CreateCurrencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("currency", 64);
            $table->string('code', 3);
            $table->smallInteger('code_num');
            $table->tinyInteger('unit')->nullable();
            $table->timestamps();
        });

        $this->populateDefault();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }

    /**
     * Populate default values
     *
     * @return void
     */
    protected function populateDefault()
    {
        DB::table('currencies')->insert([
            [
                'currency' => 'United States dollar',
                'code' => 'USD',
                'code_num' => 840,
                'unit' => 2
            ],
            [
                'currency' => 'Euro',
                'code' => 'EUR',
                'code_num' => 978,
                'unit' => 2
            ],
            [
                'currency' => 'Pound sterling',
                'code' => 'GBP',
                'code_num' => 826,
                'unit' => 2
            ],
            [
                'currency' => 'Nigerian naira',
                'code' => 'NGN',
                'code_num' => 566,
                'unit' => 2
            ],
        ]);
    }
}
