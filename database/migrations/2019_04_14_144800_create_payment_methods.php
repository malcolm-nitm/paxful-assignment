<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 64);
            $table->string('slug', 128);
            $table->timestamps();
        });

        $this->populateDefault();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_methods');
    }

    /**
     * POpulare default values
     *
     * @return void
     */
    protected function populateDefault()
    {
        $methods = [
            'Amazon gift card',
            'Walmart gift card',
            'PayPal' => 'Skrill'
        ];
        DB::table('payment_methods')->insert(array_map(function ($method) {
            return [
                'name' => $method,
                'slug' => str_slug($method)
            ];
        }, $methods));
    }
}
