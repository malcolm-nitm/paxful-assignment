<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('offers', 'OffersController');
    Route::resource('trades', 'TradesController');
    Route::post('offers/toggle', 'OffersController@toggle')->name('offers.disable');
    Route::post('trades/cancel', 'TradesController@cancel')->name('trades.cancel');
    Route::post('trades/accept', 'TradesController@accept')->name('trades.acept');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
