<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();
        static::created(function ($model) {
            $model->balance()->create([
                'balance' => 5
            ]);
        });
    }

    public function trades()
    {
        return $this->hasMany(Entities\Trade::class);
    }

    public function offers()
    {
        return $this->hasMany(Entities\Offer::class);
    }

    public function balance()
    {
        return $this->hasOne(Entities\Balance::class);
    }

    /**
     * Determine whether the given offer is owned by this user
     *
     * @param Entities\Offer $model
     * @return boolean
     */
    public function ownsOffer(Entities\Offer $model)
    {
        return $model->user_id === $this->id;
    }

    /**
     * Determine whether the given trade is owned by this user
     *
     * @param Entities\Trade $model
     * @return boolean
     */
    public function ownsTrade(Entities\Offer $model)
    {
        return $model->user_id === $this->id;
    }
}
