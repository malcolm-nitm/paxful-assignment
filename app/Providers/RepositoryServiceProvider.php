<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Repositories\TradeRepository::class, \App\Repositories\TradeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OfferRepository::class, \App\Repositories\OfferRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PaymentMethodRepository::class, \App\Repositories\PaymentMethodRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CurrencyRepository::class, \App\Repositories\CurrencyRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BalanceRepository::class, \App\Repositories\BalanceRepositoryEloquent::class);
        //:end-bindings:
    }
}
