<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class OfferValidator.
 *
 * @package namespace App\Validators;
 */
class OfferValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'final_price' => 'max:9999999999.99999999',
            'currency_id' => 'required|integer|exists:currencies,id',
            'payment_method_id' => 'integer|exists:payment_methods,id',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'final_price' => 'max:9999999999.99999999',
            'currency_id' => 'sometimes|integer|exists:currencies,id',
            'payment_method_id' => 'sometimes|exists:payment_methods,id',
        ],
    ];
}
