<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class TradeValidator.
 *
 * @package namespace App\Validators;
 */
class TradeValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'offer_id' => 'required|exists:offers,id'
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];
}
