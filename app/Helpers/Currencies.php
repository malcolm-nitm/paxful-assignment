<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

/**
 * Currency helper
 */
class Currencies
{
    public static function getRate($symbol)
    {
        $rates = static::getRates();
        return array_get($rates, 'USD'.$symbol);
    }

    public static function getRates()
    {
        return Cache::remember('currencies', Carbon::now()->addDays(1), function () {
            $url = config('currency.apiUrl');
            $key = config('currency.apiKey');
            $params = [
                '{{url}}' => $url,
                '{{query}}' => http_build_query([
                    'access_key' => $key,
                    'base' => 'USD',
                    'symbols' => 'GBP,EUR,NGN'
                ])
            ];
            $symbols = json_decode(file_get_contents(str_replace(array_keys($params), array_values($params), '{{url}}?{{query}}')), true);
            return $symbols['quotes'];
        });
    }
}
