<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Currencies;
use App\Entities\Offer;
use App\Entities\Currency;
use App\Entities\PaymentMethod;
use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\OfferCreateRequest;
use App\Http\Requests\OfferUpdateRequest;
use App\Repositories\OfferRepository;
use App\Validators\OfferValidator;

/**
 * Class OffersController.
 *
 * @package namespace App\Http\Controllers;
 */
class OffersController extends Controller
{
    /**
     * @var OfferRepository
     */
    protected $repository;

    /**
     * @var OfferValidator
     */
    protected $validator;

    /**
     * OffersController constructor.
     *
     * @param OfferRepository $repository
     * @param OfferValidator $validator
     */
    public function __construct(OfferRepository $repository, OfferValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $models = $this->repository
            ->with(['currency', 'paymentMethod', 'user'])
            ->findWhere([['user_id', '!=', \Auth::user()->id]]);

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $models,
            ]);
        }

        return view('offers.index', compact('models'));
    }

    /**
     * Show the form for creating the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Offer;
        return view('offers.create', compact('model'));
    }

    /**
     * Validatre and return a proper calculated request
     *
     * @param Request $request
     * @return void
     */
    public function calculatePrice(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'currency' => 'required|integer|exists:currencies,id',
            'method' => 'integer|exists:payment_methods,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $currencyId = $request->input('currency');
            $markup = intval($request->input('markup', 0) ?: 0);
            $result = (new Offer)->calculate($currencyId, $markup);
            return response()->json($result);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  OfferCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(OfferCreateRequest $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $model = $this->repository->create($request->all());

            $response = [
                'message' => 'Offer created.',
                'data'    => $model->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->repository->find($id);

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $model,
            ]);
        }

        return view('offers.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->repository->find($id);

        return view('offers.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  OfferUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(OfferUpdateRequest $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $model = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Offer updated.',
                'data'    => $model->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'Offer deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Offer deleted.');
    }
}
