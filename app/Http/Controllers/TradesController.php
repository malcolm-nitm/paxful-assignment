<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\TradeCreateRequest;
use App\Http\Requests\TradeUpdateRequest;
use App\Repositories\TradeRepository;
use App\Validators\TradeValidator;

/**
 * Class TradesController.
 *
 * @package namespace App\Http\Controllers;
 */
class TradesController extends Controller
{
    /**
     * @var TradeRepository
     */
    protected $repository;

    /**
     * @var TradeValidator
     */
    protected $validator;

    /**
     * TradesController constructor.
     *
     * @param TradeRepository $repository
     * @param TradeValidator $validator
     */
    public function __construct(TradeRepository $repository, TradeValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $trades = $this->repository->paginate();

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $trades,
            ]);
        }

        return view('trades.index', compact('Trades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TradeCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function checkOffer(TradeCreateRequest $request)
    {
        return response()->json($this->validator->with($request->all())->pases(ValidatorInterface::RULE_CREATE));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  TradeCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(TradeCreateRequest $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $trade = $this->repository->create($request->all());

            $response = [
                'message' => 'Trade created.',
                'data'    => $trade->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trade = $this->repository->find($id);

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $trade,
            ]);
        }

        return view('trades.show', compact('Trade'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trade = $this->repository->find($id);

        return view('trades.edit', compact('Trade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TradeUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(TradeUpdateRequest $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $trade = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Trade updated.',
                'data'    => $trade->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'Trade deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Trade deleted.');
    }
}
