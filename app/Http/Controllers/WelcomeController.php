<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\OfferRepository;

class WelcomeController extends Controller
{
    /**
     * @var OfferRepository
     */
    protected $repository;

    /**
     * OffersController constructor.
     *
     * @param OfferRepository $repository
     * @param OfferValidator $validator
     */
    public function __construct(OfferRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $query = $this->repository
            ->with(['currency', 'paymentMethod', 'user']);
        if (auth()->check()) {
            $offers = $query->findWhere([['user_id', '!=', \Auth::user()->id]]);
        } else {
            $offers = $query->get();
        }

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $offers,
            ]);
        }

        return view('welcome', compact('offers'));
    }
}
