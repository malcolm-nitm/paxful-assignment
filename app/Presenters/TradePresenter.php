<?php

namespace App\Presenters;

use App\Transformers\TradeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TradePresenter.
 *
 * @package namespace App\Presenters;
 */
class TradePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TradeTransformer();
    }
}
