<?php
namespace App\Entities;

use App\User;
use App\Helpers\Currencies;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Trade.
 *
 * @package namespace App\Entities;
 */
class Trade extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'recipient_id', 'offer_id', 'recipient_id', 'payment_method_id', 'status', 'fiat_amount'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->recipient_id = \Auth::user()->id;
            $model->offer ?? $model->load('offer');
            $model->user_id = $model->offer->user_id;
            $model->payment_method_id = $model->offer->payment_method_id;
            $model->bitcoin_amount = $model->getBitcoinAmount();
        });
    }

    /**
     * Calculate the final price
     *
     * @param integer $currencyId
     * @param integer $markup
     * @return array
     */
    public function getBitcoinAmount(): float
    {
        $currency = $this->offer->currency;
        $rate = Currencies::getRate($currency->code);
        return $this->fiat_amount / 3000 * $rate;
    }

    //
    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }
    //
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function recipient()
    {
        return $this->belongsTo(User::class);
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }
}
