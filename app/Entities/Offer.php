<?php

namespace App\Entities;

use App\User;
use App\Helpers\Currencies;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Brick\Money\Money;
use Brick\Money\Context\CustomContext;

/**
 * Class Offer.
 *
 * @package namespace App\Entities;
 */
class Offer extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'currency_id', 'payment_method_id', 'amount_min', 'amount_max', 'markup', 'final_price', 'is_active'
    ];

    protected $casts = [
        'is_active' => 'boolean',
        'markup' => 'float'
    ];

    protected $attributes = [
        'amount_min' => 1.00000000,
        'amount_max' => 1.00000000
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->user_id = $model->user_id ?? \Auth::user()->id;
            $model->amount_min = $model->amount_min ?: 1.00;
            $model->amount_max = $model->amount_max ?: 1.00;
        });
    }

    /**
     * Relations
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    /**
     * Calculate the final price
     *
     * @param integer $currencyId
     * @param integer $markup
     * @return array
     */
    public function calculate($currencyId, $markup = 0): array
    {
        $currency = Currency::find($currencyId);
        $markup = 1 + ($markup/100);
        $rate = Currencies::getRate($currency->code);
        $value = $markup * 3000 * $rate;
        return [
                'value' => $this->preparePrice($value),
                'display_value' => number_format($value, 8),
                'currency' => $currency->code
            ];
    }

    protected function preparePrice($value): string
    {
        return number_format($value, 8, '.', '');
    }

    /**
     * We need to formate the price for insertion into the database
     *
     * @param [type] $value
     * @return void
     */
    public function setFinalPriceAttribute($value)
    {
        $this->attributes['final_price'] = $this->preparePrice($value);
    }

    /**
     * We need to formate the price for display
     *
     * @param [type] $value
     * @return void
     */
    public function getFinalPriceAttribute()
    {
        return $this->preparePrice($this->attributes['final_price']);
    }

    /**
     * Get a currency formatted value for the final price
     *
     * @return string
     */
    public function getFomattedFinalPriceAttribute():string
    {
        return Money::of($this->final_price, $this->currency->code, new CustomContext(8));
    }
}
