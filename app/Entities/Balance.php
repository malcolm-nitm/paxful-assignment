<?php

namespace App\Entities;

use App\User;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Balance.
 *
 * @package namespace App\Entities;
 */
class Balance extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['balance', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
