<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Balance;

/**
 * Class BalanceTransformer.
 *
 * @package namespace App\Transformers;
 */
class BalanceTransformer extends TransformerAbstract
{
    /**
     * Transform the Balance entity.
     *
     * @param \App\Entities\Balance $model
     *
     * @return array
     */
    public function transform(Balance $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
