<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Trade;

/**
 * Class TradeTransformer.
 *
 * @package namespace App\Transformers;
 */
class TradeTransformer extends TransformerAbstract
{
    /**
     * Transform the Trade entity.
     *
     * @param \App\Entities\Trade $model
     *
     * @return array
     */
    public function transform(Trade $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
