<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TradeRepository.
 *
 * @package namespace App\Repositories;
 */
interface TradeRepository extends RepositoryInterface
{
    //
}
