<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\TradeRepository;
use App\Entities\Trade;
use App\Validators\TradeValidator;

/**
 * Class TradeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class TradeRepositoryEloquent extends BaseRepository implements TradeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Trade::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
