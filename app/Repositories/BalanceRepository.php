<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BalanceRepository.
 *
 * @package namespace App\Repositories;
 */
interface BalanceRepository extends RepositoryInterface
{
    //
}
